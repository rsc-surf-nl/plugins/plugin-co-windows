$LOGFILE = "C:\logs\webdav-mount-rdrive.log"
$rclonepath = "${env:ProgramFiles}\rclone\rclone.exe"
$winfsppath = "${env:ProgramFiles(x86)}\WinFsp\bin\fsptool-x64.exe"

Function LogWrite {
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

function Set-Culture([System.Globalization.CultureInfo] $culture) {
    [System.Threading.Thread]::CurrentThread.CurrentUICulture = $culture
    [System.Threading.Thread]::CurrentThread.CurrentCulture = $culture
}

function get-src_webdav_token($user) {
    $CO_USER_API_ENDPOINT = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud -ErrorAction SilentlyContinue).co_user_api_endpoint
    $CO_TOKEN = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud -ErrorAction SilentlyContinue).co_token
    try {
        $CO_USER_INFO = Invoke-RestMethod -Uri $CO_USER_API_ENDPOINT -Method Get -Headers @{ "Authorization" = $CO_TOKEN } -ContentType "application/json" -UseBasicParsing
    }
    catch {
        return "$_"
	    break
    }

    #RESEARCH_DRIVE_SECRET OLD key
    $RESEARCH_DRIVE_SECRET = ($CO_USER_INFO | Where-Object {$_.username -eq $user}).research_drive_secret

    #RESEARCH_DRIVE_CONFIG NEW key
    $RESEARCH_DRIVE_CONFIG = (($CO_USER_INFO | Where-Object {$_.username -eq $user}).services | Where-Object {$_.service_meta.category -eq "research_drive"})
    if ($RESEARCH_DRIVE_CONFIG) {
        # Access elements of $myArray here
        $RESEARCH_DRIVE_CONFIG = $RESEARCH_DRIVE_CONFIG[0]
    }

    #WEBDAV_DRIVE_CONFIG
    $WEBDAV_DRIVE_CONFIG = (($CO_USER_INFO | Where-Object {$_.username -eq $user}).services | Where-Object {$_.service_meta.category -eq "webdav"})
    if ($WEBDAV_DRIVE_CONFIG) {
        # Access elements of $myArray here
        $WEBDAV_DRIVE_CONFIG = $WEBDAV_DRIVE_CONFIG[0]
    }

    $CO_USER_INFO = New-Object PSObject
    
    if ($RESEARCH_DRIVE_SECRET.token) {
        $CO_USER_INFO | Add-Member -Name RESEARCH_DRIVE_CONFIG -Value $RESEARCH_DRIVE_SECRET -MemberType NoteProperty
    } elseif ($RESEARCH_DRIVE_CONFIG.research_drive) {
        $CO_USER_INFO | Add-Member -Name RESEARCH_DRIVE_CONFIG -Value $RESEARCH_DRIVE_CONFIG.research_drive -MemberType NoteProperty
    }

    if ($WEBDAV_DRIVE_CONFIG.webdav) {
       $CO_USER_INFO | Add-Member -Name WEBDAV_DRIVE_CONFIG -Value $WEBDAV_DRIVE_CONFIG -MemberType NoteProperty
    }

    return $CO_USER_INFO
}

function Get-UserSession {
	[CmdLetBinding(DefaultParameterSetName = "Default")]
	param (
		[string]$ComputerName = $env:COMPUTERNAME,
		[Parameter(ParameterSetName = "Active")]
		[switch]$Active,
		[switch]$Console,
		[Parameter(ParameterSetName = "Disconnected")]
		[switch]$Disconnected
	)

	$sessions = (&"query" user /SERVER:$ComputerName) -replace "^[\s]USERNAME[\s]+SESSIONNAME.*$", "" -replace "[\s]{2,}", "," -replace ">", "" |
		ConvertFrom-Csv -Delimiter "," -Header "Username", "SessionName", "Id", "State", "IdleTime", "LogonTime"

	foreach ($session in $sessions) {
		Add-Member -InputObject $session -MemberType NoteProperty -Name "ComputerName" -Value $ComputerName
	}

	if ($Active) {
		$sessions = $sessions | Where-Object { $_.State -eq "Active" }
	}
	elseif ($Disconnected) {
		$sessions = $sessions | Where-Object { $_.State -eq "Disc" }
	}

	if ($Console) {
		$sessions = $sessions | Where-Object { $_.SessionName -eq "console" }
	}
	
	return $sessions
}

Function Main { 
    $lastuserlogin = Get-LocalUser | Sort-Object -Property LastLogon -Descending | Select-Object -Property * -First 1
    $qusers = Get-UserSession

    #Checking installed software
    if (!(Test-Path $rclonepath)) {
        LogWrite("rclone not found in $rclonepath")
        break
    }

    if (!(Test-Path $winfsppath)) {
        LogWrite("winfsp not found in $rclonepath")
        break
    }

    #set date en time for the system account
    $culture = Get-Culture
    $culture.DateTimeFormat.ShortDatePattern = 'dd/MM/yyyy'
    $culture.DateTimeFormat.FullDateTimePattern = 'dddd, d MMMM yyyy HH:mm:ss'
    $culture.DateTimeFormat.LongDatePattern = 'dddd, d MMMM yyyy'
    $culture.DateTimeFormat.MonthDayPattern = 'd MMMM'
    $culture.DateTimeFormat.FirstDayOfWeek = 'Monday'
    Set-Culture $culture

    #Check users that are active on the server
    foreach ($quser in $qusers) {
        #check if the last logged-in user matches with the active user
        if ($quser.Username -eq $lastuserlogin.Name) {
            $active_user = $quser
            break
        }
    }
    if ($active_user) {
        $quserlogontime = Get-Date $active_user.LogonTime
        $timeSpan = New-TimeSpan -Start $quserlogontime -End (Get-Date)

        if ($timeSpan.TotalMinutes -lt 1) {
            
            $user = $lastuserlogin.Name
            $profilesRootDir =  Get-ItemPropertyValue 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList' ProfilesDirectory
            $userprofilefolder = $profilesRootDir+"\"+$user
            $src_webdav_token = get-src_webdav_token $user
            $rclone_conf_rd = "$userprofilefolder\AppData\Roaming\rclone\rclone_rdrive.conf"
            $rclone_conf_wd = "$userprofilefolder\AppData\Roaming\rclone\rclone_webdav.conf"
            $RESEARCH_DRIVE_CONFIG = $src_webdav_token.RESEARCH_DRIVE_CONFIG
            $WEBDAV_DRIVE_CONFIG = $src_webdav_token.WEBDAV_DRIVE_CONFIG

            if ($RESEARCH_DRIVE_CONFIG) {
                if ($RESEARCH_DRIVE_CONFIG.webdav_token) {
                    $RESEARCH_DRIVE_SECRET_token = $($RESEARCH_DRIVE_CONFIG.webdav_token)
                } else {
                    $RESEARCH_DRIVE_SECRET_token = $($RESEARCH_DRIVE_CONFIG.token)
                }

                if ($RESEARCH_DRIVE_SECRET_token) {
                    $encrypted_password = & $rclonepath obscure $RESEARCH_DRIVE_SECRET_token

                    New-Item -ItemType File -Path $rclone_conf_rd -Force

                    Clear-Content $rclone_conf_rd
                    Add-Content $rclone_conf_rd "[RD]"
                    Add-Content $rclone_conf_rd "type = webdav"
                    if ($($RESEARCH_DRIVE_CONFIG.url).Remove(0, ($RESEARCH_DRIVE_CONFIG.url.Length - 1)) -eq '/') {
                        Add-Content $rclone_conf_rd "url = $($RESEARCH_DRIVE_CONFIG.url)"
                    } else {
                        Add-Content $rclone_conf_rd "url = $($RESEARCH_DRIVE_CONFIG.url)/"
                    }
                    Add-Content $rclone_conf_rd "vendor = owncloud"
                    Add-Content $rclone_conf_rd "user = $($RESEARCH_DRIVE_CONFIG.username)"
                    Add-Content $rclone_conf_rd "pass = $encrypted_password"

                    LogWrite("Research Drive rclone config has been set for $user")
                }
            }
            
            if ($WEBDAV_DRIVE_CONFIG.webdav.webdav_token) {

                $encrypted_password = & $rclonepath obscure $WEBDAV_DRIVE_CONFIG.webdav.webdav_token

                New-Item -ItemType File -Path $rclone_conf_wd -Force

                Clear-Content $rclone_conf_wd
                Add-Content $rclone_conf_wd "[WebDAV]"
                Add-Content $rclone_conf_wd "type = webdav"

                Write-Host $WEBDAV_DRIVE_CONFIG.webdav.url

                if ($($WEBDAV_DRIVE_CONFIG.webdav.url).Remove(0, ($WEBDAV_DRIVE_CONFIG.webdav.url.Length - 1)) -eq '/') {
                    Add-Content $rclone_conf_wd "url = $($WEBDAV_DRIVE_CONFIG.webdav.url)"
                } else {
                    Add-Content $rclone_conf_wd "url = $($WEBDAV_DRIVE_CONFIG.webdav.url)/"
                }
                Add-Content $rclone_conf_wd "vendor = owncloud"
                Add-Content $rclone_conf_wd "user = $($WEBDAV_DRIVE_CONFIG.webdav.username)"
                Add-Content $rclone_conf_wd "pass = $encrypted_password"

                LogWrite("rclone config has been set for $user")
            }

            if (!($RESEARCH_DRIVE_CONFIG)) {
                LogWrite ("No ResearchDrive config found for user $user")
                if (Test-Path -Path $rclone_conf_rd) {
                    #removing config file researchdrive
                    remove-item -Path $rclone_conf_rd -Force
                }
            }

            if (!($WEBDAV_DRIVE_CONFIG)) {
                LogWrite ("No Webdav config found for user $user")
                if (Test-Path -Path $rclone_conf_wd) {
                    #removing config file for webdav
                    remove-item -Path $rclone_conf_wd -Force
                }
            }
        }
    }

}

main