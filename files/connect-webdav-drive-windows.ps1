$LOGFILEPATH = "$env:USERPROFILE\WebDAV\LOGS\"
$LOGFILE = $LOGFILEPATH + "src-mount-webdav.log"
$rclonepath = "${env:ProgramFiles}\rclone\rclone.exe"
$rclone_conf_rdrive = "$env:APPDATA\rclone\rclone_rdrive.conf"
$rclone_conf_webdav = "$env:APPDATA\rclone\rclone_webdav.conf"

Function LogWrite
{
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

Function Update-ACL($function,$objACL,$Rights,$InheritanceFlag,$PropagationFlag,$objType,$objUser) {
    $objACLch = Get-ACL $objACL

    if ($function -eq "add" -or $function -eq "del") {
        $Rights = [System.Security.AccessControl.FileSystemRights]$Rights
        if ($InheritanceFlag -eq "none") {
            $InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]::$InheritanceFlag
        } else {
            $InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]$InheritanceFlag
        }
        if ($PropagationFlag -eq "none") {
            $PropagationFlag = [System.Security.AccessControl.PropagationFlags]::$PropagationFlag
        } else {
            $PropagationFlag = [System.Security.AccessControl.PropagationFlags]$PropagationFlag
        }
        $objType =[System.Security.AccessControl.AccessControlType]::$objType
        $objUser = New-Object System.Security.Principal.NTAccount($objUser)
        
        if ($function -eq "add") {
            $objACE = New-Object System.Security.AccessControl.FileSystemAccessRule($objUser, $Rights, $InheritanceFlag, $PropagationFlag, $objType) 
            $objACLch.AddAccessRule($objACE)
        }

        if ($function -eq "del") {
            $acesToRemove = $objACLch.Access | Where-Object{ $_.FileSystemRights -eq $Rights -and $_.InheritanceFlags -eq $InheritanceFlag -and $_.PropagationFlags -eq $PropagationFlag -and $_.AccessControlType -eq $objType }
            $objACLch.RemoveAccessRule($acesToRemove)
        }
    }
    if ($function -eq "rminherited") {
        $objACLch.SetAccessRuleProtection($true,$false)
    }

    #Set-ACL $objACL $objACLch
    (Get-Item $objACL).SetAccessControl($objACLch)
   # (Get-Item c:\path\to\folder).SetAccessControl()
}

Function rclone_mount($rclonepath, $rcloneconfig, $drive, $webdavname) {
    
    $Mounted_folders = @()


    #mounting with rclone for webdav
    if ($rcloneconfig -eq $rclone_conf_webdav) {
        $rcloneLOGFILE = "$env:USERPROFILE\WebDAV\LOGS\rcloneWebDavLOG.log"
    } elseif ($rcloneconfig -eq $rclone_conf_rdrive) {
        $rcloneLOGFILE = "$env:USERPROFILE\WebDAV\LOGS\rcloneRDriveLOG.log"
    }

    if (Test-Path -Path $rcloneLOGFILE) {
        Remove-Item -Path $rcloneLOGFILE -Force
    }
    
    #create new local webdav folder
    $webdavlocalroot = "$home\WebDAV"
    $webdavlocalhome = "$webdavlocalroot\$webdavname"

    if ($webdavname -eq "Research Drive") {
        #create a local RDrive folder
        New-Item -Path $webdavlocalhome -ItemType Directory -Force | Out-Null

        #Allow change permissions as current user with read only
        Update-ACL "add" $webdavlocalhome "ChangePermissions, TakeOwnership, Synchronize" "ContainerInherit, ObjectInherit" "None" "Allow" "$env:username"
        #Current user allow write access to this folder only
        Update-ACL "add" $webdavlocalhome "Read, ReadAndExecute, Write, Synchronize" "None" "None" "Allow" "$env:username"
        #Set current user fullControl access to subfolders and files
        Update-ACL "add" $webdavlocalhome "FullControl" "ContainerInherit, ObjectInherit" "InheritOnly" "Allow" "$env:username"
        #Allow system account full controll
        Update-ACL "add" $webdavlocalhome "FullControl" "ContainerInherit, ObjectInherit" "None" "Allow" "NT AUTHORITY\SYSTEM"
        #removing inherited permissions
        Update-ACL "rminherited" $webdavlocalhome

        #get all webdav root folders
        $allrootwebdavfolders = & $rclonepath --config ""$rcloneconfig"" lsf RD:

        if ($allrootwebdavfolders) {
            LogWrite("Recived the following $allrootwebdavfolders to mount")
        } else {
            LogWrite("No sub-folders found in the $webdavname mount check mount error in this log file $rcloneLOGFILE")
        }

        #get current rclone config folder 
        $fileContentconfig = Get-Content $rcloneconfig -Raw

        #connect each folder to webdav root
        foreach ($rootwebdavfolderName in $allrootwebdavfolders) {
            if ($rootwebdavfolderName[-1] -eq "/") {
                $rootwebdavfolderName = $rootwebdavfolderName.Substring(0,$rootwebdavfolderName.Length-1)
            
                $webdavconfigpath = Get-Item $rcloneconfig
                $newwdconfig = $webdavconfigpath.DirectoryName+'\'+$webdavconfigpath.Basename
                $newwdconfigeach = $newwdconfig+"_$rootwebdavfolderName.conf"

                $rcloneLOGFILEbase = (Split-Path -Path $rcloneLOGFILE -Leaf).Split(".")[0];
                $rcloneLOGFILEeach = $LOGFILEPATH + $rcloneLOGFILEbase + "_" + $rootwebdavfolderName + ".log"

                $newwebdavprojectfolder = $webdavlocalhome + "\" +$rootwebdavfolderName
            
                #create a new config file with the folder name
                New-Item -ItemType File -Path $newwdconfigeach -Force | Out-Null
                Clear-Content $newwdconfigeach
                $pattern = 'url = (https?:\/\/[^ ].*\/)'
                $newconfigfile = $fileContentconfig -replace $pattern,"url = `$1$rootwebdavfolderName"
                Set-Content -Path $newwdconfigeach -Value $newconfigfile

                #start a rclone process per folder

                $rcloneprocess = Start-Process -FilePath $rclonepath -ArgumentList "--config ""$newwdconfigeach"" mount RD: ""$newwebdavprojectfolder"" --vfs-cache-mode writes --use-cookies --log-file=""$rcloneLOGFILEeach""" -WindowStyle Hidden -PassThru
                $Mounted_folders += $newwebdavprojectfolder

                #waiting for process to start
                Start-Sleep 1
            }
        }
        if($Mounted_folders.count -gt 0) {
            #make mapping:
            $webdavlocalhomesh = ($webdavlocalhome -replace '^*:', '$')
            $rclonedriveletter = $drive

            if (!(Test-Path -Path $rclonedriveletter)) {
                NET USE $rclonedriveletter "\\localhost\$webdavlocalhomesh" | Out-Null
                $a = New-Object -ComObject shell.application
                $a.NameSpace( "$rclonedriveletter" ).self.name = $webdavname
            }

            # Create shortcut on desktop
            if (!(Test-Path -Path "$Home\Desktop\\$webdavname.lnk")) {
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut("$Home\Desktop\\$webdavname.lnk")
                $Shortcut.TargetPath = "$rclonedriveletter"
                $Shortcut.Save()
            }
            LogWrite("WebDAV successfully mounted as drive letter $rclonedriveletter!")
        }
    }

    if ($webdavname -eq "WebDAV") {
        #get first availible drive letter
        $rclonedriveletter = ('ZYXWVUTSQPONMLKJIHGF'.ToCharArray() | Where-Object { $_ -notin ([System.IO.DriveInfo]::GetDrives().Name).Substring(0,1) })[0] + ":"
        #start a rclone process per to mount
        $rcloneLOGFILEbase = (Split-Path -Path $rcloneLOGFILE -Leaf).Split(".")[0];
        $rcloneLOGFILEeach = $LOGFILEPATH + $rcloneLOGFILEbase + "_" + "WebDAV" + ".log"
        $rcloneprocess = Start-Process -FilePath $rclonepath -ArgumentList "--config ""$rcloneconfig"" mount WebDAV: $rclonedriveletter --vfs-cache-mode writes --use-cookies --log-file=""$rcloneLOGFILEeach""" -WindowStyle Hidden -PassThru
        $Mounted_folders += $rclonedriveletter

        # Create shortcut on desktop
        if (!(Test-Path -Path "$rclonedriveletter")) {
            $WshShell = New-Object -comObject WScript.Shell
            $Shortcut = $WshShell.CreateShortcut("$Home\Desktop\\$webdavname.lnk")
            $Shortcut.TargetPath = "$rclonedriveletter"
            $Shortcut.Save()
        }
    }
    return $Mounted_folders
}

function check_mount($mountpoint, $webdavname) {
    #wait for volume get mounted
    $mounted = 0
    while ( $mounted -ne 6 ) {
        try {
            $tmp_file = "$mountpoint\src_$(Get-Date -Format "MM-dd-yyyy-HH.mm").tmp"
            New-Item -Path $tmp_file -ErrorAction Stop -WarningAction SilentlyContinue -Force
            If (Test-Path $tmp_file)
            {
                $mountedcheck = $true
                Remove-Item -Path $tmp_file -Force
            }
        }
        catch {
            $mountedcheck = $false
        }

        if ($mountedcheck) {
            #find mounted driveletter from log
            LogWrite("$mountpoint mounted")
            $mounted = 6
        } else {
            if ($mounted -eq 5) {
                LogWrite("Unable to mount $webdavname to $mountpoint")
                break
            }
            LogWrite("$mountpoint still trying to mount")
            Start-Sleep -Seconds 5
            $mounted++
        }
    }
}

Function Main { 
    write-host "The LOG file can be found here: $LOGFILE"

    if (!($(Test-Path $LOGFILEPATH))) {
        New-Item -Path $LOGFILEPATH -ItemType Directory -Force
    }
    #check if the $rclone_conf file is created and waiting for the file to exist
    $configfile=$false
    For ($i = 0; $i -lt 10) {
        if ((Test-Path $rclone_conf_rdrive) -or (Test-Path $rclone_conf_webdav))  {
            #Config file has been found
            $i=10
            $configfile = $true
        } else {
            $i++
            Start-Sleep -Seconds 5
        }
    }
    
    if ($configfile) {
        if (Test-Path $rclone_conf_rdrive) {
            #mounting with rclone for rdrive
            $mountedfolders = rclone_mount $rclonepath $rclone_conf_rdrive "R:" "Research Drive"

            #check if all folders have been mounted
            foreach ($mountedfolder in $mountedfolders) {
            $mountedfolder
                check_mount $mountedfolder "Research Drive"
            }

            #remove one user specific rights and allow only read right
            $webdavlocalhome = "$home\WebDAV\Research Drive"
            Update-ACL "del" $webdavlocalhome "Read, ReadAndExecute, Write, Synchronize" "None" "None" "Allow" "$env:username"
            Update-ACL "add" $webdavlocalhome "Read, ReadAndExecute, Synchronize" "None" "None" "Allow" "$env:username"

        }
        
        if (Test-Path $rclone_conf_webdav) {
            #mounting with rclone for webdav
            $mountedfolders = rclone_mount $rclonepath $rclone_conf_webdav "*" "WebDAV"

            #check if all folders have been mounted
            foreach ($mountedfolder in $mountedfolders) {
                check_mount $mountedfolder "WebDAV"
            }
        } 
    } else {
        LogWrite("No webdav config not found")
        # remove mapping
        if (net use r:) {
            net use r: /d | Out-Null
        }
        # remove desktop shortcut if exist
        if (test-path "$Home\Desktop\\WebDav.lnk") {
            Remove-Item -Path "$Home\Desktop\\WebDav.lnk" -Force
            LogWrite("Desktop shutcut WebDav has been removed")
        }
        # remove desktop shortcut if exist
        if (test-path "$Home\Desktop\\Research Drive.lnk") {
            Remove-Item -Path "$Home\Desktop\\Research Drive.lnk" -Force
            LogWrite("Desktop shutcut Research Drive has been removed")
        }
        # remove Research Drive folder
        $rmrdfolder = "$home\WebDAV\Research Drive"
        if (Test-Path $rmrdfolder) {
            Remove-Item -Path $rmrdfolder -Force
        }
    }
}

Main